//Austin Lordi
//Hw04 
//Rolls dice in yahtzee game and tells you your score

import java.util.Scanner;

public class Yahtzee {
   public static void main(String[] args) {
     
     int dice1, dice2, dice3, dice4, dice5;
     
     Scanner myScanner = new Scanner( System.in );
     System.out.print("Enter 1 to roll random numbers and enter 2 to choose your numbers: ");
     int useInput = myScanner.nextInt();
     
     //if user chooses 1, it sets up a random number generator for each dice
     if ( useInput == 1 ){
        dice1 = (int) (Math.random() * 6) + 1;
        dice2 = (int) (Math.random() * 6) + 1;
        dice3 = (int) (Math.random() * 6) + 1;
        dice4 = (int) (Math.random() * 6) + 1;
        dice5 = (int) (Math.random() * 6) + 1;
     }
     //if user chooses 2 they can enter their own numbers
     else {
        System.out.print("Enter the numbers of the 5 dice as a 5 digit number: ");
        myScanner.nextLine();
        String inputDice = myScanner.nextLine();
        int inputDiceNumber = Integer.parseInt(inputDice); // converts the string into an integer
       if( inputDice.length() != 5 ){ //error if the user does not input a 5 digit number
         System.out.println("Error, not 5 dice.");
         return;
       }
       
       //how to find the number of each dice from user entered numbers, by dividing to find the number in each place
       dice1 = inputDiceNumber / 10000;
       dice2 = (inputDiceNumber%10000) / 1000;
       dice3 = (inputDiceNumber%1000) / 100;
       dice4 = (inputDiceNumber%100) / 10;
       dice5 = (inputDiceNumber%10);
       
       
       //error if user enters a number that is not 1 through 6 
       if (dice5 > 6 || dice4 > 6 || dice3 > 6 || dice2 > 6 || dice1 > 6 || 
         dice5 < 1 || dice4 < 1 || dice3 < 1 || dice2 < 1 || dice1 < 1){
        System.out.println("Error, dice values out of range.");
         return;
       }
     }
     
     //lets you know what you rolled on your dice
     System.out.println("Dice 1 = " + dice1);
     System.out.println("Dice 2 = " + dice2);
     System.out.println("Dice 3 = " + dice3);
     System.out.println("Dice 4 = " + dice4);
     System.out.println("Dice 5 = " + dice5);
     
     
     int diceAdded = dice1 + dice2 + dice3 + dice4 + dice5; //dice values added up
     int upperTotal = diceAdded;//the sum of the dice is also the value of the upper total (before bonus)
     int bonus = 0; 
     //bonus of 35 if the uppertotal value is greater than 63
     if( upperTotal >= 63 ){
       bonus = 35;
     }
     int upperWBonus = upperTotal + bonus; //value of the uppertotal added with the bonus
     
     // starting values for the number of times each number (1-6) is rolled on the dice
     int numOf1s = 0; 
     int numOf2s = 0;
     int numOf3s = 0;
     int numOf4s = 0;
     int numOf5s = 0;
     int numOf6s = 0;
     
     //switches to added up the number of times each number is rolled, repeated for dice (1-5)
     switch(dice1){
       case 1:
         numOf1s = numOf1s + 1;
         break;
       case 2:
         numOf2s = numOf2s + 1;
         break;
       case 3:
         numOf3s = numOf3s + 1;
         break;
       case 4:
         numOf4s = numOf4s + 1;
         break;
       case 5:
         numOf5s = numOf5s + 1;
         break;
       case 6:
         numOf6s = numOf6s + 1;
         break;
     }    
       
     switch(dice2){
       case 1:
         numOf1s = numOf1s + 1;
         break;
       case 2:
         numOf2s = numOf2s + 1;
         break;
       case 3:
         numOf3s = numOf3s + 1;
         break;
       case 4:
         numOf4s = numOf4s + 1;
         break;
       case 5:
         numOf5s = numOf5s + 1;
         break;
       case 6:
         numOf6s = numOf6s + 1;
         break;   
     }
     switch(dice3){
       case 1:
         numOf1s = numOf1s + 1;
         break;
       case 2:
         numOf2s = numOf2s + 1;
         break;
       case 3:
         numOf3s = numOf3s + 1;
         break;
       case 4:
         numOf4s = numOf4s + 1;
         break;
       case 5:
         numOf5s = numOf5s + 1;
         break;
       case 6:
         numOf6s = numOf6s + 1;
         break;
     }
     switch(dice4){
       case 1:
         numOf1s = numOf1s + 1;
         break;
       case 2:
         numOf2s = numOf2s + 1;
         break;
       case 3:
         numOf3s = numOf3s + 1;
         break;
       case 4:
         numOf4s = numOf4s + 1;
         break;
       case 5:
         numOf5s = numOf5s + 1;
         break;
       case 6:
         numOf6s = numOf6s + 1;
         break;
     }
     switch(dice5){
       case 1:
         numOf1s = numOf1s + 1;
         break;
       case 2:
         numOf2s = numOf2s + 1;
         break;
       case 3:
         numOf3s = numOf3s + 1;
         break;
       case 4:
         numOf4s = numOf4s + 1;
         break;
       case 5:
         numOf5s = numOf5s + 1;
         break;
       case 6:
         numOf6s = numOf6s + 1;
         break;
     }
     
     int lowerTotal = 0;//initializes lower total counter
     
     //3 of a kind
     if( numOf1s == 3 ){
       lowerTotal = lowerTotal + (3 * 1);
     }
     else if( numOf2s == 3 ){
       lowerTotal = lowerTotal + (3 * 2);
     }
     else if( numOf3s == 3 ){
       lowerTotal = lowerTotal + (3 * 3);
     }
     else if( numOf4s == 3 ){
       lowerTotal = lowerTotal + (3 * 4);
     }
     else if( numOf5s == 3 ){
       lowerTotal = lowerTotal + (3 * 5);
     }
     else if( numOf6s == 3 ){
       lowerTotal = lowerTotal + (3 * 6);
     }
     
     //4 of a kind
     if( numOf1s == 4 ){
       lowerTotal = lowerTotal + (4 * 1);
     }
     else if( numOf2s == 4 ){
       lowerTotal = lowerTotal + (4 * 2);
     }
     else if( numOf3s == 4 ){
       lowerTotal = lowerTotal + (4 * 3);
     }
     else if( numOf4s == 4 ){
       lowerTotal = lowerTotal + (4 * 4);
     }
     else if( numOf5s == 4 ){
       lowerTotal = lowerTotal + (4 * 5);
     }
     else if( numOf6s == 4 ){
       lowerTotal = lowerTotal + (4 * 6);
     }
     
              
     //full house
     if( (numOf1s == 3 || numOf2s == 3 || numOf3s == 3 || numOf4s == 3 || numOf5s == 3 || numOf6s == 3) && 
         (numOf1s == 2 || numOf2s == 2 || numOf3s == 2 || numOf4s == 2 || numOf5s == 2 || numOf6s == 2) ){
       lowerTotal = lowerTotal + 25; // +25 for full house
     }
     
     
     //small straight
     //covers most 1234 combos
     if( numOf1s >= 1 && numOf2s >= 1 && numOf3s >= 1 && numOf4s >= 1 && numOf1s <= 2 && numOf2s <= 2 && numOf3s <= 2
        && numOf4s <= 2 && numOf5s == 0 && numOf6s == 0 ){
       lowerTotal = lowerTotal + 30;
     }
     //covers 12346
     if( numOf1s == 1 && numOf2s == 1 && numOf3s == 1 && numOf4s == 1 && numOf5s == 0 && numOf6s == 1 ){
       lowerTotal = lowerTotal + 30;
     }
     //covers 2345 combos
     if( numOf2s >= 1 && numOf3s >= 1 && numOf4s >= 1 && numOf5s >= 1 && numOf2s <= 2 && numOf3s <= 2 && numOf4s <= 2 && numOf5s <= 2 && numOf1s == 0 && numOf6s == 0 ){
       lowerTotal = lowerTotal + 30;
     }
     //covers most 3456 combos
     if( numOf3s >= 1 && numOf4s >= 1 && numOf5s >= 1 && numOf6s >= 1 && numOf3s <= 2 && numOf4s <= 2 && numOf5s <= 2 && numOf6s <= 2 && numOf1s == 0 && numOf2s == 0 ){
       lowerTotal = lowerTotal + 30;
     }
     //covers 13456
     if( numOf1s == 1 && numOf2s == 0 && numOf3s == 1 && numOf4s == 1 && numOf5s == 1 && numOf6s == 1 ){
       lowerTotal = lowerTotal + 30;
     }
     
     
     //long straight
     //first scenario: 12345
     if( numOf1s == 1 && numOf2s == 1 && numOf3s == 1 && numOf4s == 1 && numOf5s == 1 && numOf6s == 0 ){
       lowerTotal = lowerTotal + 40; //+40 for a long straight
     }
     //other scenario: 23456
     if( numOf1s == 0 && numOf2s == 1 && numOf3s == 1 && numOf4s == 1 && numOf5s == 1 && numOf6s == 1 ){
       lowerTotal = lowerTotal + 40; // same reason as above
     }
     
     //YAHTZEE - 5 of a kind
     if( numOf1s == 5 || numOf2s == 5 || numOf3s == 5 || numOf4s == 5 || numOf5s == 5 || numOf6s == 5 ){
       lowerTotal = lowerTotal + 50; // +50 for yahtzee
     }
     
     //chance - adds the total of all five dice
     lowerTotal = lowerTotal + diceAdded;
     
     int grandTotal = upperWBonus + lowerTotal;
     
     System.out.println("The upper section initial total is " + upperTotal);
     System.out.println("The upper section total with bonus is " + upperWBonus);
     System.out.println("The lower section total is " + lowerTotal);
     System.out.println("The grand total is " + grandTotal);
     
     
     
     
     
       
     
     
     
     
        
     
   
     
     
   }
}