// Austin Lordi
// 2/4/2018
// HW 02
// The purpose is to store and compute the cost of items you bought at a store. 
public class Arithmetic {
  public static void main(String[] args) {


    //Number of pairs of pants
    int numPants = 3;
    //Cost per pair of pants
    double pantsPrice = 34.98;

    //Number of sweatshirts
    int numShirts = 2;
    //Cost per shirt
    double shirtPrice = 24.99;

    //Number of belts
    int numBelts = 1;
    //cost per box of envelopes
    double beltCost = 33.99;

    //the tax rate
    double paSalesTax = 0.06;
    
    double totalCostOfPants; // total cost of pants
    double totalCostOfShirts; // total cost of sweatshirts
    double totalCostOfBelts; // total cost of belts
    double pantsTax, beltTax, shirtTax; // sales tax for each type of item
    double totalCostOfPurchases; // total cost of purchases
    double totalSalesTax, totalPaid; // total cost of sales tax and the total amount paid
  
    
    totalCostOfPants = numPants * pantsPrice;
    totalCostOfBelts = numBelts * beltCost;
    totalCostOfShirts = numShirts * shirtPrice;
    
    pantsTax = ((int)((totalCostOfPants * paSalesTax) * 100)) / 100.0;
    beltTax = ((int)((totalCostOfBelts * paSalesTax) * 100)) / 100.0;
    shirtTax = ((int)((totalCostOfShirts * paSalesTax) * 100)) / 100.0;
    
    totalCostOfPurchases = totalCostOfShirts + totalCostOfBelts + totalCostOfPants;
    totalSalesTax = ((int)((totalCostOfPurchases * paSalesTax) * 100)) / 100.0;
    totalPaid = ((int)((totalCostOfPurchases + totalSalesTax) * 100)) / 100.0;
    
    System.out.println("The total cost of pants is $" + totalCostOfPants);
    System.out.println("The total cost of belts is $" + totalCostOfBelts);
    System.out.println("The total cost of shirts is $" + totalCostOfShirts);
    System.out.println("The sales tax of pants is $" + pantsTax);
    System.out.println("The sales tax of belts is $" + beltTax);
    System.out.println("The sales tax of shirts is $" + shirtTax);
    
    System.out.println("The total cost of purhcases is $" + totalCostOfPurchases);
    System.out.println("The total sales tax is $" + totalSalesTax);
    System.out.println("The total cost of the transaction is $" + totalPaid);
    
    
  }
}