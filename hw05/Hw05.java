// Austin Lordi
// Hw05


import java.util.Scanner;
public class Hw05 {
   public static void main(String[] args) {
     
     Scanner myScanner = new Scanner( System.in );
     int courseNumber; // declares course number
     
     while (true){ // starts infinite loop
        System.out.print("Enter the course number: ");
        if (myScanner.hasNextInt()){
          courseNumber = myScanner.nextInt(); // assigns course number to integer entered 
          if (courseNumber > 0) //only breaks infinite loop if the number is both an integer and positive
            break;
        }
       else{
       String junkWord = myScanner.next(); // deletes what was entered 
       }
       System.out.println("Please a enter a positive integer."); // asks user to enter number again
     }  
     
     String departmentName; // initializes department name
     while(true){
        System.out.print("Enter the department name: ");
        if (myScanner.hasNextInt() || myScanner.hasNextDouble()){
           String junkWord = myScanner.next(); // deletes and asks again if user enters an integer or a double
        }
       else{
       departmentName = myScanner.next(); //breaks loop if user enters string
         break;
       }
        System.out.println("Please enter a name."); //asks user to reenter
     }
     
     int numOfMeets; // initializes number of meetings
     while (true){
        System.out.print("Enter the number of meetings per week: ");
        if (myScanner.hasNextInt()){
          numOfMeets = myScanner.nextInt(); // assigns to integer
          if (numOfMeets > 0){ //only breaks infinite loop if the number is both an integer and positive
            break;
          }
        }
       else{
       String junkWord = myScanner.next();
       }
       System.out.println("Please a enter a positive integer."); // asks user to reenter
     }  
     
     int hour, minute; // initializes two separate integers for hour and minute
     
     while (true){
        System.out.print("Enter the time the class starts (use a space for a colon): ");
        if (myScanner.hasNextInt()){ //the hour is the first part of the number entered, before the space
          hour = myScanner.nextInt();
          if (hour >= 1 && hour <= 12 && myScanner.hasNextInt()){ // hour must be in between 1 and 12
            minute = myScanner.nextInt(); // minute is the part after the space
            if (minute >= 00 && minute <= 59){ // minute must be between 0 and 59
              break;
            }
          }
        }
       else{
       String junkWord = myScanner.next();
       }
       System.out.println("Please a enter a positive integer that is in the range of a clock"); //asks user to reenter
     }  
     
     String instructorName; //initializes string for instructor name (last name only)
      while(true){
        System.out.print("Enter the instructor's last name: ");
        if (myScanner.hasNextInt() || myScanner.hasNextDouble()){ //deletes if user enters integer or double
           String junkWord = myScanner.next();
        }
       else{
       instructorName = myScanner.next(); //assigns instructor name to string entered
         break;
       }
        System.out.println("Please enter a name: "); //asks user to reenter
     }
     
     int numOfStudents; // initializes number of students
      while (true){
        System.out.print("Enter the number of students: ");
        if (myScanner.hasNextInt()){
          numOfStudents = myScanner.nextInt(); // assigns to integer
          if (numOfStudents > 0) //only breaks infinite loop if the number is both an integer and positive
            break;
        }
       else{
       String junkWord = myScanner.next();
       }
       System.out.println("Please a enter a positive integer."); //asks user to reenter
     }  
     
     System.out.println("Course number: " + courseNumber);
     System.out.println("Department name: " + departmentName);
     System.out.println("Number of meetings per week: " + numOfMeets);
     System.out.println("Class start time: " + hour + ":" + minute);
     System.out.println("Instructor name: " + instructorName);
     System.out.println("Number of students: " + numOfStudents); 
     
     
     
     
     
     
     
     
     
     
     
     
     
     
   }
}
