//Austin Lordi
//HW10

import java.util.Random;
public class RobotCity{
  
  public static int[][] buildCity(){//build city method
    Random rand = new Random();
    
    int EW = rand.nextInt(6) + 10; //generates number from 10 to 15
    int NS = rand.nextInt(6) + 10;
    
    int[][]cityArray = new int[NS][EW];
    
    for(int a = 0; a < NS; a++){
      cityArray[a] = new int[EW];
      for(int b = 0; b < EW; b++){
        cityArray[a][b] = rand.nextInt(900) + 100; //assigns number from 100 to 999 for each part of array
      }
    }
    
    return cityArray;
  }
  
  public static void display(int[][]cityArray){ //display method
     for (int a = 0; a < cityArray.length; a++){ // prints in grid format
        for (int b = 0; b < cityArray[a].length; b++){
          System.out.printf("%4d ",cityArray[a][b]);
        }
        System.out.println();
      }
  }
  
  public static void invade(int[][]cityArray, int k){ // invade method
    Random rand = new Random();
    for (int a = 0; a < k; a++){
      int x = rand.nextInt(cityArray[0].length); // assigns random coordinates within the height and width of the array
      int y = rand.nextInt(cityArray.length);
      cityArray[y][x] = -cityArray[y][x]; //makes the population negative
    }
  }
  
  public static void update(int[][]cityArray){ //update array
    for (int a = 0; a < cityArray.length; a++){
      for(int b = cityArray[a].length - 1; b >= 0; b--){ //array in reverse so that aliens will not run over each other
        if (cityArray[a][b] < 0){
          cityArray[a][b] = -cityArray[a][b]; 
          if(b + 1 < cityArray[a].length){
            cityArray[a][b+1] = -cityArray[a][b+1];//shifts the alien one tile to the east
          }
        }
      }
    }
  }
  
  
  
  
  public static void main(String[] args){
   Random rand = new Random();
   int k = rand.nextInt(40) + 10; //k is random integer from 10 to 50
    
    
   int cityArray[][];
   cityArray = buildCity();
   display(cityArray);
   System.out.println();
   invade(cityArray, k);
   display(cityArray);
   System.out.println();
   for (int a = 0; a < 5; a++){ //runs update 5 times
     update(cityArray);
     display(cityArray);
     System.out.println();
   }
}
}