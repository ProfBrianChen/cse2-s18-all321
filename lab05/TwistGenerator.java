// Austin Lordi
// Lab05
// The purpose is to print out a twist that is the length given by the numbers entered


import java.util.Scanner;
public class TwistGenerator {
   public static void main(String[] args) {
     
     Scanner myScanner = new Scanner( System.in );
     System.out.print("Enter the length: "); // asks user to enter length of twist
     int length = 0; // starts length counter
     
     while (true){ // starts infinite loop
        if (myScanner.hasNextInt()){
          length = myScanner.nextInt(); // assigns to integer
          if (length > 0)
            break; // only breaks loop if what the user entered is a positive integer
        }
       else{
       String junkWord = myScanner.next(); //deletes what was entered
       }
       System.out.print("Reenter the length: "); //asks user to reenter
     }  
    
     int counter = 0; //intiliazes counter
     
     while (counter < length){
       if(counter%3 == 0){ // if remainder when length is divided by 3 is 0, it prints a \ because its 1st char in 3 char sequence
          System.out.print("\\");
       }
       if(counter%3 == 1){
          System.out.print(" "); // prints space when rem is 1, it is 2nd in sequence
       }
       if(counter%3 == 2){
          System.out.print("/"); // prints / when rem is 2
       }
       counter++; // adds 1 to counter and loop repeats
     }   
     counter = 0; // resets counter
     System.out.println(); // starts a new line
     while (counter < length){
       if(counter%3 == 0){
          System.out.print(" "); // 1st symbol in sequence of 3
       }
       if(counter%3 == 1){
          System.out.print("X"); // 2nd symbol in sequence of 3
       }
       if(counter%3 == 2){
          System.out.print(" "); // 3rd symbol
       }
       counter++; // adds 1 to counter and loop repeats
     }   
     counter = 0; // resets counter
     System.out.println(); // new line
     while (counter < length){
       if(counter%3 == 0){
          System.out.print("/"); // 1st symbol
       }
       if(counter%3 == 1){
          System.out.print(" ");// 2nd symbol
       }
       if(counter%3 == 2){
          System.out.print("\\");// 3rd symbol
       }
       counter++; // adds 1 to counter and loop repeats
     }   
     System.out.println();
   
     
     
     
     
     
     
   }
}
