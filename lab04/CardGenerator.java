//Austin Lordi
//Lab04 2/16/18
//The purpose of this lab is to create a random number generator that tells
//you which card in a deck of cards you chose. 



public class CardGenerator {
   public static void main(String[] args) {
     
    
     int cardNumber = (int) (Math.random() * 52) + 1;// random number generator, + 1 to account for zero and 1
     
     String suit = "";// starts string for card suit, diamonds, hearts, etc.. 
     String cardType = "";// starts string for card type, 1 through king
     
     if (cardNumber >= 1 && cardNumber <= 13){suit = "Diamonds";}//range for diamonds
     else if ( cardNumber > 13 && cardNumber <= 26){suit = "Clubs";}// range for clubs
     else if ( cardNumber > 26 && cardNumber <= 39){suit = "Hearts";}//range for hearts
     else if ( cardNumber > 39 && cardNumber <= 52){suit = "Spades";}//range for spades
     
     
    switch (cardNumber%13){//divides card number by 13 and finds remainder
      case 1:
        cardType = "ace";
          break;
      case 2: 
        cardType = "2";
          break;
      case 3:
        cardType = "3";
          break;  
      case 4:
        cardType = "4";
        break;
      case 5: 
        cardType = "5";
        break;
      case 6:
        cardType = "6";
        break;
      case 7:
        cardType = "7";
        break;
      case 8:
        cardType = "8";
        break;
      case 9: 
        cardType = "9";
        break;
      case 10:
        cardType = "10";
        break;
      case 11:
        cardType = "Jack";
        break;
      case 12:
        cardType = "Queen";
        break;
      case 0: 
        cardType = "King";
        break;
      default:
        cardType = "Not sure";
        break;
    }
     
     System.out.println("You picked the " + cardType + " of " + suit);
     
     
     
     
     
     
   }
}
