// Austin Lordi 
// Lab02 2/2/2018 
// CSE002 
// The purpose of this program is to print the number of minutes for each trip, 
// the number of counts for each trip, the distance of each trip in miles
// print, and the distance for the two trips combined

public class Cyclometer {
    	// main method required for every Java program
   	public static void main(String[] args) {
			int secsTrip1=480;  // the number of seconds in the first trip
			int secsTrip2=3220;  // the number of seconds in the second trip
			int countsTrip1=1561;  // the number of counts in the first trip
			int countsTrip2=9037; // the number of counts in the second trip

			double wheelDiameter=27.0,  // diameter of the wheel
			PI=3.14159, // value of pi
			feetPerMile=5280,  // conversion for number of feet per mile
			inchesPerFoot=12,   // conversion for number of inches per foot
			secondsPerMinute=60;  // conversion for number of seconds per minute
			// distance of trip 1, distance of trip, total distance is distancetrip1 + distancetrip2
			double distanceTrip1, distanceTrip2,totalDistance;  
			
			System.out.println("Trip 1 took "+
					(secsTrip1/secondsPerMinute)+" minutes and had "+
					 countsTrip1+" counts.");
			System.out.println("Trip 2 took "+
					(secsTrip2/secondsPerMinute)+" minutes and had "+
					 countsTrip2+" counts.");

			distanceTrip1=countsTrip1*wheelDiameter*PI;
			// Above gives distance in inches
			//(for each count, a rotation of the wheel travels
			//the diameter in inches times PI)
			distanceTrip1/=inchesPerFoot*feetPerMile; // Gives distance in miles
			distanceTrip2=countsTrip2*wheelDiameter*PI/inchesPerFoot/feetPerMile;
			totalDistance=distanceTrip1+distanceTrip2;

			//Print out the output data.
			System.out.println("Trip 1 was "+distanceTrip1+" miles");
			System.out.println("Trip 2 was "+distanceTrip2+" miles");
			System.out.println("The total distance was "+totalDistance+" miles");


	}  //end of main method   
} //end of class



