//Austin Lordi
//Lab09

public class lab09 {
  
  public static int[] copy(int[]list){
    final int number = list.length;
    int[] newList = new int[number];
    
    for (int a = 0; a < number; a++){
      newList[a] = list[a];
    }
    
    return newList;
  }
  
  public static void inverter(int[]list){
      for (int a = 0; a < list.length/2; a++){
        int b = list[a];
        list[a] = list[list.length - a - 1];
        list[list.length - a - 1] = b;
      }
    
  }
  
  public static int[] inverter2(int[]list){
    final int number = list.length;
    int[] copyList = new int[number];
    copyList = copy(list);
    int[] invList = new int[number];
    
    inverter(copyList);
    
    return copyList;
  }
  
  public static void print(int[]list){
    for (int a = 0; a < list.length; a++){
      System.out.print(list[a] + " ");
    }
  }
 
  public static void main(String[] args) {
    final int number = 8;
    int[] array0 = new int[number];
    array0[0] = 0;
    array0[1] = 1;
    array0[2] = 2;
    array0[3] = 3;
    array0[4] = 4;
    array0[5] = 5;
    array0[6] = 6;
    array0[7] = 7;
    
    int[] array1 = new int[number];
    array1 = copy(array0);
    
    int[] array2 = new int[number];
    array2 = copy(array0);
    
    inverter(array0);
    print(array0);
    System.out.println();
    
    inverter2(array1);
    print(array1);
    System.out.println();
    
    int[]array3 = new int [number];
    array3 = inverter2(array2);
    print(array3);
    System.out.println();
  }
}

    
    
    