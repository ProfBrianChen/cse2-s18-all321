//Austin Lordi
//HW08

import java.util.Scanner;
import java.util.Random;
public class cse {
  
    public static void binary(int[] grades, int searchNumber) {
      int start = 0;
      int end = grades.length - 1;
      int middle = (end - start) / 2;
      int counter = 1;
      
      while (start < end){
        if (searchNumber == grades[middle]){
            break;
        }
        else if (searchNumber > grades[middle]){
          start = middle + 1;
        }
        else {
          end = middle - 1;
        }
        middle = start + ((end - start) / 2);
        counter++;
      }
      if (searchNumber == grades[middle]){
        System.out.println(searchNumber + " was found with " + counter + " iterations.");
      }
      else {
        System.out.println(searchNumber + " was not found in the list with " + counter + " iterations. ");
      }
    
    
    }
  
    public static void linear(int[] grades, int searchNumber2) {
      for ( int a = 0; a < grades.length; a++){
        if (searchNumber2 == grades[a]) {
          System.out.println(searchNumber2 + " was found.");
          return;
        }
      }
      System.out.println(searchNumber2 + " was not found.");
      
    }
  
   public static void random(int[]grades) {
     Random rand = new Random();
     for (int a = 0; a < grades.length; a++){
       int i = rand.nextInt(grades.length);
       int j = rand.nextInt(grades.length);
       int k = grades[i];
       grades[i] = grades[j];
       grades[j] = k;
     }
     System.out.println("Scrambled: ");
     for (int b = 0; b < grades.length; b++){
       System.out.print(grades[b] + " ");
     }
      System.out.println();
   }
  
  
  
  
   	public static void main(String[] args) {
      
      final int number = 15;
      int[] grades;
      grades = new int[number];
      int a;
      int c;
      Scanner myScanner = new Scanner( System.in ); 
      
      while(true){
      System.out.println("Enter 15 ascending ints for final grades in CSE2: ");
      for (a = 0; a < 15; a++){
        if (myScanner.hasNextInt()){
          c = myScanner.nextInt();
          if (c >= 0 && c <= 100){
             grades[a] = c; 
          }
          else{
            System.out.println("Error. Grades entered are out of range.");
            break;
          }
        }
        else{
          System.out.println("Error. Enter all integers.");
          break;
        }
      
      }
      if (a < 15){
        continue;
      }
      for (a = 1; a < 15; a++){
         if (grades[a] < grades[a-1]){
           System.out.println("Error. Grades entered are not in ascending order.");
           break;
         }
      } 
      if (a >= 15){
        break;
      }
      }
      for (int b = 0; b < 15; b++){
        System.out.print(grades[b] + " ");
      }
      System.out.println();
      
      System.out.print("Enter a grade to search for: ");
      int searchNumber = myScanner.nextInt();
      
      binary(grades, searchNumber);
      random(grades);
      
      System.out.print("Enter a grade to search for: ");
      int searchNumber2 = myScanner.nextInt();
      
      linear(grades, searchNumber2);
     
      
      
      
      
    
          
      
      
      
      
      
      
    }
}
