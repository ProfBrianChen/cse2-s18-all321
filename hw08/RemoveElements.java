//Austin Lordi
//Hw08 Program 2


import java.util.Random;
import java.util.Scanner;
public class RemoveElements{
  
  public static int[] randomInput() { //random number method
    Random rand = new Random();
    final int number = 10;
    int[] randIntegers; //random number array
    randIntegers =  new int[number];
    
    for (int a = 0; a < number; a++){
      randIntegers[a] = rand.nextInt(10); //generates random number from 0 to 9 for each part of the array
    }
    
    return randIntegers;
  }
  
  public static int[] delete(int[]list, int pos) { // delete method
    final int number = list.length - 1;
		int[] newList; // new array with a length 1 shorter than the previous list
		newList = new int[number];
			
		for (int b = 0; b < pos; b++){ 
			newList[b] = list[b]; // sets the new array equal to the old array for every number before the one that is deleted
		}	
		for ( int a = pos; a < number; a++){
      newList[a] = list[a+1]; // assigns the new array to the next number in the old array to skip the one deleted
    }
		return newList;
  }
  
  public static int[] remove(int[]list, int target) {// remove array
		final int number = list.length;
		int[] newList; //creates new list
		newList = new int[number];
		int b = 0;
		
    for (int a = 0; a < list.length; a++){
      if (list[a] != target){ 
        newList[b] = list[a]; //assigns new list to old list only if the number js not the target
				b++; //sets the length of the new array
      }
    }
		
		int[] returnList = new int[b]; //new array with the length figured out earlier
	  for (int c = 0; c < b; c++){
			returnList[c] = newList[c]; 
		}
		return returnList;
  }
  
  
  
  
  
  
  public static void main(String [] arg){
	Scanner scan=new Scanner(System.in);
int num[]=new int[10];
int newArray1[];
int newArray2[];
int index,target;
	String answer="";
	do{
  	System.out.print("Random input 10 ints [0-9]");
  	num = randomInput();
  	String out = "The original array is:";
  	out += listArray(num);
  	System.out.println(out);
 
  	System.out.print("Enter the index ");
  	index = scan.nextInt();
  	newArray1 = delete(num,index);
  	String out1="The output array is ";
  	out1+=listArray(newArray1); //return a string of the form "{2, 3, -9}"  
  	System.out.println(out1);
 
      System.out.print("Enter the target value ");
  	target = scan.nextInt();
  	newArray2 = remove(num,target);
  	String out2="The output array is ";
  	out2+=listArray(newArray2); //return a string of the form "{2, 3, -9}"  
  	System.out.println(out2);
  	 
  	System.out.print("Go again? Enter 'y' or 'Y', anything else to quit-");
  	answer=scan.next();
	}while(answer.equals("Y") || answer.equals("y"));
  }
 
  public static String listArray(int num[]){
	String out="{";
	for(int j=0;j<num.length;j++){
  	if(j>0){
    	out+=", ";
  	}
  	out+=num[j];
	}
	out+="} ";
	return out;
  }
}
