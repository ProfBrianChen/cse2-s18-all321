//Austin Lordi
//Hw09

import java.util.Random;
public class DrawPoker {
  
  public static boolean pair(int[] player){ //pair method
    for (int a = 0; a < player.length; a++){ //tests each card
      int test = player[a];
      for (int b = 0; b < player.length; b++){
        if(test%13 == player[b]%13 && test != player[b]){ //returns true if card finds a match
          return true;
      }
    }
    }
    return false;
  }
  
  public static boolean threeOfKind(int[] player){// 3 of a kind method
    for (int a = 0; a < player.length; a++){ // tests each card
      int test = player[a];
      for (int b = 0; b < player.length; b++){
        if(test%13 == player[b]%13){ //finds first match
          int test2 = player[b];
          for (int c = 0; c < player.length; c++){
            if(test2%13 == player[c]%13 && player[c] != test && player[c] != test2 && test2 != test){//returns true if finds a second match
              return true;
             }
          }
        }
      }
    }
    return false;
  }
  
  public static boolean flush(int[] player){ //flush method
    int test = player[0];
    int counter = 0; //starts counter
    for (int a = 1; a < player.length; a++){
      if(test/13 == player[a]/13){
        counter++; //adds counter if card is the same suit as the first card
      }
    }
    
    if (counter == 4){ // returns true if every card had the same suit
      return true;
    }
    else{
      return false;
  }
  }
  
  public static boolean fullHouse(int[]player){ //fullhouse method
    int counter = 0;//starts counter
    for (int a = 0; a < player.length; a++){
      int test = player[a];
      for(int b = 0; b < player.length; b++){
        if (test%13 == player[b]%13 && test != player[b]){ //adds counter if it finds a match
          counter++;
          break;
        }
      }
    }
    if (counter == 5){ //returns true if every card found a match
      return true;
    }
    else {
      return false;
    }
  }
  
  public static int high(int[]player){ //method for highest number
    int max = 0;
    for (int a = 0; a < player.length; a++){ // goes through every number and replaces it if it is larger
      if (player[a]%13 > max){
        max = player[a]%13;
      }
    }
    return max;
  }
    
  
  public static void main(String[] args) { 
    final int number = 52;
    int[]cards = new int[number];
    
    for (int a = 0; a < 52; a++){
      cards[a] = a; //sets array 1 to 52
    }
    
    //shuffles array of cards
    Random rand = new Random();
    for (int a = 0; a < 51; a++){ // switches one time for every number in the array
       int i = rand.nextInt(51);
       int j = rand.nextInt(51);
       int k = cards[i]; // switches i and j 
       cards[i] = cards[j];
       cards[j] = k;
     }
    
    final int handNum = 5;
    int[]player1 = new int[handNum];
    int[]player2 = new int[handNum];
    
    for (int a = 0; a < 5; a++){ //sets the players' hands to every other card 
      player1[a] = cards[2 * a];
      player2[a] = cards[2 * a + 1];
    }
    
    
    
    System.out.print("Player 1's Cards: ");
    for (int a = 0; a < 5; a++){
      System.out.print(player1[a] + " ");
    }
    System.out.println();
    
    System.out.print("Player 2's Cards: ");
    for (int a = 0; a < 5; a++){
      System.out.print(player2[a] + " ");
    }
    System.out.println();
    
    System.out.println("Pair: " + pair(player1) + " " + pair(player2));
    System.out.println("3 of kind: " + threeOfKind(player1) + " " + threeOfKind(player2));
    System.out.println("Flush: " + flush(player1) + " " + flush(player2));
    System.out.println("Full House: " + fullHouse(player1) + " " + fullHouse(player2)); 
    
    int rank1 = 0; //ranks to determine who won - higher numbers are more important 
    int rank2 = 0;
    if (pair(player1)){
      rank1 = rank1 + 1;
    }
    if (pair(player2)){
      rank2 = rank2 + 1;
    }
    if(threeOfKind(player1)){ // adds additional 1 to the 1 added from already being a pair
      rank1 = rank1 + 1;
    }
    if(threeOfKind(player2)){
      rank2 = rank2 + 1;
    }
    if(flush(player1)){
      rank1 = rank1 + 3;
    }
    if(flush(player2)){
      rank2 = rank2 + 3;
    }
    if(fullHouse(player1)){
      rank1 = rank1 + 5;
    }
    if(fullHouse(player2)){
      rank2 = rank2 + 5;
    }
    
    if(rank1 > rank2){
    System.out.println("Player 1 Wins");
    }
    
    if(rank2 > rank1){
    System.out.println("Player 2 Wins");
    }
    
    if(rank1 == rank2 && rank1 != 0 && rank2 != 0){
      System.out.println("The game is a tie");
    }
    
    if (rank1 == 0 && rank2 == 0){
      if (high(player1) > high(player2)){
        System.out.println("Player 1 Wins");
      }
      else if(high(player2) > high(player1)){
        System.out.println("Player 2 Wins");
      }
      else {
        System.out.println("The game is a tie");
      }
    }
    
    
  }
}