//Austin Lordi
//Lab08

import java.util.Random;
import java.util.Scanner;
public class lab08 {
   public static void main(String[] args) {
     
     Random randomGenerator = new Random();
     final int number = randomGenerator.nextInt(6) + 5;
     String[] students;
     students = new String[number];
     
     for (int a=0; a < number; a++){
       Scanner myScanner = new Scanner( System.in );
       System.out.println("Enter the name of the student: ");
       students[a] = myScanner.next();
     }
     
     int[] midterm;
     midterm = new int[number];
     
     for (int b=0; b < number; b++){
       Random randomGenerator2 = new Random();
       midterm[b] = randomGenerator2.nextInt(101);
     }
     
     System.out.println("Here are the midterm grades of the " + number + " students above: ");
     for(int c = 0; c < number; c++){
        System.out.println(students[c] + ": " + midterm[c]);
     }
     
   }
}

   
     