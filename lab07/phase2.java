// Austin Lordi
// Lab07

import java.util.Random;
import java.util.Scanner;
public class phase2 {
  
  public static String adjectiveMeth(){
    Random randomGenerator = new Random();
    int adjint = randomGenerator.nextInt(10);
    String adjective = "";
    switch (adjint){
      case 0: adjective = "quick";
        break;  
      case 1: adjective = "brown";
        break;
      case 2: adjective = "lazy";
        break;
      case 3: adjective = "hungry";
        break;
      case 4: adjective = "fat";
        break;
      case 5: adjective = "incredible";
        break;
      case 6: adjective = "wise";
        break;
      case 7: adjective = "green";
        break;
      case 8: adjective = "scary";
        break;
      case 9: adjective = "blue";
        break;
    }
      return adjective;
  }
  public static String subjectMeth(){
    Random randomGenerator = new Random();
    int subint = randomGenerator.nextInt(10);
    String subject = "";
    switch (subint){
      case 0: subject = "fox";
        break;
      case 1: subject = "monkey";
        break;
      case 2: subject = "rhinoceros";
        break;
      case 3: subject = "unicorn";
        break;
      case 4: subject = "monster";
        break;
      case 5: subject = "dentist";
        break;
      case 6: subject = "vampire";
        break;
      case 7: subject = "penguin";
        break;
      case 8: subject = "polar bear";
        break;
      case 9: subject = "soldier";
        break;
    }
      return subject;
  }
  public static String verbMeth(){
    Random randomGenerator = new Random();
    int verbint = randomGenerator.nextInt(10);
    String verb = "";
    switch (verbint){
      case 0: verb = "passed";
        break;  
      case 1: verb = "attacked";
        break;
      case 2: verb = "ran";
        break;
      case 3: verb = "jumped";
        break;
      case 4: verb = "burned";
        break;
      case 5: verb = "destroyed";
        break;
      case 6: verb = "smelled";
        break;
      case 7: verb = "offended";
        break;
      case 8: verb = "poked";
        break;
      case 9: verb = "hugged";
        break;
    }
      return verb; 
  }
  public static String objectMeth(){
    Random randomGenerator = new Random();
    int objint = randomGenerator.nextInt(10);
    String object = "";
    switch (objint){
      case 0: object = "dog";
        break;
      case 1: object = "cat";
        break;
      case 2: object = "moose";
        break;
      case 3: object = "plane";
        break;
      case 4: object = "car";
        break;
      case 5: object = "train";
        break;
      case 6: object = "turtle";
        break;
      case 7: object = "shoe";
        break;
      case 8: object = "elephant";
        break;
      case 9: object = "tree";
        break;
    }    
      return object;
  }
  
  public static String thesis() {
    String subject = subjectMeth();
    System.out.println("The " + adjectiveMeth() + " " + adjectiveMeth() + " " + subject + " " +
       verbMeth() + " the " + adjectiveMeth() + " " + objectMeth() + ".");
   return subject;
    
  }
  public static void action(String subject) {
    Random randomGenerator = new Random();
    System.out.println("This " + subject + " was " + adjectiveMeth() + " to " + adjectiveMeth() + " " + objectMeth() + "s.");
    int subject2int = randomGenerator.nextInt(2);
     String subject2 = "";
     switch (subject2int){
      case 0: subject2 = "The " + subject;
        break;  
      case 1: subject2 = "It";
        break;
     }
    System.out.println(subject2 + " " + verbMeth() + " " + objectMeth() + "s with " + 
       objectMeth() + "s at the " + adjectiveMeth() + " " + objectMeth() + ".");
  }
  
  public static void conclusion(String subject) {
    System.out.println("That " + subject + " " + verbMeth() + " her " + objectMeth() + "s!");
  }
  
  public static void paragraph() {
    Random randomGenerator = new Random();
    String subject = thesis();
    int numOfSentences = randomGenerator.nextInt(4);
    for (int i = 0; i < numOfSentences; i++) {
      action(subject);
    }
    conclusion(subject);
  }
    
    
   public static void main(String[] args) {
  paragraph();
 
     
     
   }
}