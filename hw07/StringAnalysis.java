// Austin Lordi
// Hw07 String Analysis


import java.util.Scanner;
import java.lang.Character;
public class StringAnalysis {
   
  public static boolean count(String name, int number) {//method for when the user chooses the number of letters to be checked
   for (int i = 0; i < number; i++){
      if (!Character.isLetter(name.charAt(i))){//returns false if there is a character that is not a number
        return false;
      }
    }
    return true;// returns true if all numbers
  }
  
  public static boolean count(String name) {//method that check the whole string
    for (int i = 0; i < name.length(); i++){
      if (!Character.isLetter(name.charAt(i))){//returns false if there is a character that is not a number
        return false;
      }
    }
    return true; // returns true if all numbers
  }
  
  
  public static void main(String[] args) {
   Scanner myScanner = new Scanner( System.in );
   boolean result = true;
    System.out.print("Enter a string. ");
      String name = myScanner.next();
    System.out.print("Enter the number of characters to check. Enter 0 to check all numbers. ");
      int number = myScanner.nextInt();
    if (number <=0 || number >= name.length()){//if the user enters a number not is not in the range of the string, it will check the whole string
      result = count(name);
    }
    else {
      result = count(name, number); //happens if the user enters a usable number
    }
    
    if (result == true){
      System.out.println("This string has all letters.");
    }
    else {
      System.out.println("This string contains numbers.");
    }
      
     
     
     
     
     
     
     
   }
}