// Austin Lordi
// Hw07 Area


import java.util.Scanner;
import java.lang.Math;
public class Area {
   
  public static double triangle(double base, double height) {//method for triangle area
    double triArea;
    triArea = base * height * 0.5; // formula for area of a triangle
    return triArea;
  }
  
  public static double rectangle(double length, double width) { //method for rectangle area
    double rectArea;
    rectArea = length * width; //formula for rectangle area
    return rectArea;
  }
  
  public static double circle(double radius){ //method for circle area
    double circArea;
    circArea = Math.PI * radius * radius; //formula for circle area
    return circArea;
  }

  
  public static void input(){ //method for inputs
    Scanner myScanner = new Scanner( System.in );
    while (true){
      System.out.print("Enter a shape: ");
      String shape = myScanner.next();
      if (shape.equals("triangle")){ // tests if what the user typed in is "triangle"
        System.out.print("Enter the base: "); //assigns base and height
        double base = myScanner.nextDouble();
        System.out.print("Enter the height: ");
        double height = myScanner.nextDouble();
        double triArea = triangle(base, height); //calls the triangle area from triangle method
        System.out.println("The area of the triangle is " + triArea);
        break;
      }
      else if (shape.equals("circle")){ //checks for "circle"
        System.out.print("Enter the radius: ");
        double radius = myScanner.nextDouble(); // assigns radius
        double circArea = circle(radius);//calls the circle method to get the area
        System.out.println("The area of the circle is " + circArea);
        break;
      }
      else if (shape.equals("rectangle")){ //checks for "rectangle"
        System.out.print("Enter the length: "); //assigns the length and width
        double length = myScanner.nextDouble();
        System.out.print("Enter the width: ");
        double width = myScanner.nextDouble();
        double rectArea = rectangle(length, width);//calls the rectangle method to get the area
        System.out.println("The area of the rectangle is " + rectArea);
        break;
      }
      else {
        System.out.println("This shape is invalid. Please enter triangle, rectangle, or circle.");
        // asks the user to enter again if what they entered is not a correct shape
      }
      
    
    }
    
  }
  
  
  
  public static void main(String[] args) {
   input();   //calls the input method
     
     
     
     
     
     
     
   }
}