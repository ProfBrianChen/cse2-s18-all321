public class WelcomeClass {
   public static void main(String[] args) {
      // Prints "Hello, World" in the terminal window.
      System.out.println("    -----------");
      System.out.println("    | WELCOME |");
      System.out.println("    -----------");
      System.out.println("  ^  ^  ^  ^  ^  ^");
      System.out.println(" / \\/ \\/ \\/ \\/ \\/ \\");
      System.out.println("<-A--L--L--3--2--1->");
      System.out.println(" \\ /\\ /\\ /\\ /\\ /\\ /");
      System.out.println("  v  v  v  v  v  v");     
      System.out.println("Hello, my name is Austin Lordi and I am from Lansdale, Pennsylvania and am currently \na freshmen engineering student at Lehigh University enrolled in the course CSE 002.");
   }
}