//Austin Lordi
//HW03

import java.util.Scanner;
public class Convert {
   public static void main(String[] args) {
     
     Scanner myScanner = new Scanner( System.in );
     System.out.print("Enter the affected area in acres: ");
     double area = myScanner.nextDouble();
     double convertedArea = area / 640; //affected area converted to miles
     
     System.out.print("Enter the rainfall in the affected area in inches: ");
     double aveRainfall = myScanner.nextDouble();
     double convertedRain = aveRainfall / 63360; //rainfall converted to miles
     
     double totalRain;
     
     totalRain = convertedArea * convertedRain;
     
     System.out.println(totalRain + " cubic miles");
     
     
     
   }
}